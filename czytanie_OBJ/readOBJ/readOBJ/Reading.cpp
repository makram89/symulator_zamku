#include<iostream>
#include<fstream>
#include<string>
#include"OBJ_Loader.h"

int main() {
	objl::Loader read;
	
	std::string vertexName = "vertex";
	std::string normalsName = "normals";
	std::string texCoordsName = "texCoords";
	if (read.LoadFile("C:/Users/marek/Documents/symulator_zamku/modele_zamku/Medieval_props/beczki.obj")) {
		int i = 0;
		for (objl::Mesh mesh : read.LoadedMeshes) {
			std::ofstream vertex, normals, texCoords;
			vertex.open(vertexName + std::to_string(i) + ".txt");
			normals.open(normalsName + std::to_string(i) + ".txt");
			texCoords.open(texCoordsName + std::to_string(i) + ".txt");
			i++;
			//objl::Mesh mesh = read.LoadedMeshes[i];
			for (objl::Vertex v : mesh.Vertices) {
				vertex << v.Position.X << " " << v.Position.Y << " " << v.Position.Z << " " << std::to_string(1.0) << " ";
				normals << v.Normal.X << " " << v.Normal.Y << " " << v.Normal.Z << " " << std::to_string(1.0) << " ";
				texCoords << v.TextureCoordinate.X << " " << v.TextureCoordinate.Y << " " << std::to_string(1.0) << " ";
			}
			vertex.close();
			normals.close();
			texCoords.close();
		}
	}
	
	system("pause");
	return 0;
}