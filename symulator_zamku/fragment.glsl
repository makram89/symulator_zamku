#version 330

//Zmienne interpolowane


in vec4 l2; //interpolowany wektor "do światła" w przestrzeni oka

in float i_power;
in float i_power2;

in vec4 n; //interpolowany wektor normalny w przestrzeni oka

in vec2 iTexCoord0; //interpolowane współrzędne teksturowania dla tekstury w textureMap0

uniform sampler2D textureMap0; //Jednostka teksturująca 0

out vec4 pixelColor; //Zmienna wyjsciowa fragment shadera. Zapisuje sie do niej ostateczny (prawie) kolor piksela

uniform vec4 C_direction;
uniform float radius; //cos



void main(void) {
	

	

    vec4 kd=texture(textureMap0,iTexCoord0);  //Kolor materia?u dla ?wiat?a  rozpraszanego
    vec4 ld=vec4(0.8,0.8,0.8,1); //Kolor ?wiat?a  rozpraszanego

    vec4 ks = texture(textureMap0, iTexCoord0)*0.8f;				//kolor odbijanego materialu (tekstury) z osłabieniem

    
	
	vec4 ls3=vec4(1,0.0f,0.0f,1.0f); //Kolor ?wiat?a odbijanego z okna
	vec4 ld3=vec4(1,0,0,1.0f); //Kolor swiatla  rozpraszanego(kolor swiatla)



	//primitive spotlight
	
	float nl3 = 0.0f;
	float rv3 = 0.0f;
	float fi = dot(l2,normalize(-C_direction));
	if(fi > radius)
	{
		nl3 = 0.5f;
		rv3 = 0.5f;

	}


	float ambient = 0.1f;
	

	pixelColor=vec4(((kd.rgb * ld.rgb * ambient ) + (kd.rgb*i_power) + kd.rgb*i_power2)/3 + kd.rgb * ld3.rgb * nl3 +  + ks.rgb * ls3.rgb * rv3, kd.a);

}
