#include "castle.h"



castle::castle(){}
castle::~castle(){}
glm::vec3 read3floats(std::string line) {
	glm::vec3 v;
	std::istringstream buffer(line);
	std::string word;
	buffer >> word;
	buffer >> word;
	v.x = stof(word);
	buffer >> word;
	v.y = stof(word);
	buffer >> word;
	v.z = stof(word);
	return v;
}
glm::vec2 read2floats(std::string line) {
	glm::vec2 v;
	std::istringstream buffer(line);
	std::string word;
	buffer >> word;
	buffer >> word;
	v.x = stof(word);
	buffer >> word;
	v.y = stof(word);
	return v;
}

void castle::read_vertexes(std::string path) {
	std::ifstream read;
	read.open(path);
	std::vector<float> vertexes;
	std::string vertex;
	while (!read.eof()) {
		read >> vertex;
		vertexes.push_back(std::stof(vertex));
	}
	tabVertex = new float[vertexes.size()];
	vertexCount = vertexes.size();
	long long int i = 0;
	for (float vert : vertexes) {
		tabVertex[i++] = vert;
	}
}
void castle::read_normals(std::string path) {
	std::ifstream read;
	read.open(path);
	std::vector<float> normals;
	std::string normal;
	while (!read.eof()) {
		read >> normal;
		normals.push_back(std::stof(normal));
	}
	tabNormals = new float[normals.size()];
	long long int i = 0;
	for (float norm : normals) {
		tabNormals[i++] = norm;
	}
}
void castle::read_texCoords(std::string path) {
	std::ifstream read;
	read.open(path);
	std::vector<float> texCoords;
	std::string texCoord;
	while (!read.eof()) {
		read >> texCoord;
		texCoords.push_back(std::stof(texCoord));
	}
	tabTexCoords = new float[texCoords.size()];
	long long int i = 0;
	for (float tCoord : texCoords) {
		tabTexCoords[i++] = tCoord;
	}
}

void castle::read_all_models(std::string path, int nr) {
	std::vector<float> vertexes, normals, texCoords;
	long long int k = 0;
	
	for (int i = 0; i <= nr; i++) {
		model oneModel;
		oneModel.vertexCount = 0;
		std::ifstream readVert, readNorm, readTCoord;
		std::string vertex, normal, texCoord;
		readVert.open(path + "vertex" + std::to_string(i) + ".txt");
		readNorm.open(path + "normals" + std::to_string(i) + ".txt");
		readTCoord.open(path + "texCoords" + std::to_string(i) + ".txt");
		while (!readVert.eof()) {
			
			readVert >> vertex;
			vertexes.push_back(stof(vertex));
		}
		while (!readNorm.eof()) {
			readNorm >> normal;
			normals.push_back(stof(normal));
		}
		while (!readTCoord.eof()) {
			readTCoord >> texCoord;
			texCoords.push_back(stof(texCoord));
		}
		oneModel.vertexCount = (vertexes.size()) /4;
		oneModel.tabVertexes = new float[vertexes.size()];
		oneModel.tabNormals = new float[normals.size()];
		oneModel.tabTexCoords = new float[texCoords.size()];
		for (std::vector<float>::iterator v = vertexes.begin(); v != vertexes.end(); v++) {
			oneModel.tabVertexes[k++] = *v;
		}
		k = 0;
		int counter = 0;
		for (std::vector<float>::iterator n = normals.begin(); n != normals.end(); n++) {
			if (counter == 3) {
				counter = 0;
				oneModel.tabNormals[k++] = 0.0f;
				
			}
			else {
				oneModel.tabNormals[k++] = *n;
				counter++;
			}
		}
		k = 0;
		counter = 0;
		for (std::vector<float>::iterator t = texCoords.begin(); t != texCoords.end(); t++) {
			if (counter == 2) {
				counter = 0;
				continue;
			}
			else {
				oneModel.tabTexCoords[k++] = *t;
				counter++;
			}
		}
		k = 0;
		Models.push_back(oneModel);
		
		vertexes.clear();
		normals.clear();
		texCoords.clear();
		readVert.close();
		readNorm.close();
		readTCoord.close();
	}
}