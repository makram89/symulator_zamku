#version 330

//Zmienne jednorodne
uniform mat4 P;
uniform mat4 V;
uniform mat4 M;


uniform vec4 lp2; //Współrzędne źródła światła w przestrzeni świata

uniform float lpower; //Współrzędne źródła światła w przestrzeni świata
uniform float l2power; //Współrzędne źródła światła w przestrzeni świata


//Atrybuty
//in vec4 vertex; //wspolrzedne wierzcholka w przestrzeni modelu
//in vec4 normal; //wspolrzedne wektora normalnego w przestrzeni modelu
//in vec2 texCoord0; //współrzędna teksturowania

layout (location=0) in vec4 vertex; //wspolrzedne wierzcholka w przestrzeni modelu
layout (location=1) in vec4 normal; //wektor normalny w wierzcholku  w przestrzeni modelu
layout (location=2) in vec2 texCoord0; //wspó³rzêdne teksturowania





//spotlight
out vec4 l2;

out vec4 n;
out float i_power;
out float i_power2;

out vec2 iTexCoord0; //ttex coordy i_Tc


void main(void) {




    l2=normalize((lp2-M*vertex)); //spot light -> nie ruszac


	mat4 G=mat4(inverse(transpose(mat3(M))));
    n=normalize(V*G*normal); //Wektor normalny w przestrzeni oka M -> G
    i_power = clamp(dot(n,normalize(vec4(0,0,1,0))),0,1) * lpower;
    i_power2 = clamp(dot(n,normalize(vec4(0,1,-1,0))),0,1) * l2power;
	
    
    iTexCoord0=texCoord0; //Zapewnienie interpolacji współrzędnych teksturowania podanych poprzez atrybut
  
    
    gl_Position=P*V*M*vertex;
}