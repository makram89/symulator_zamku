#pragma once
#pragma warning(disable : 4996)
#include<string>
#include<GL/glew.h>
#include<GLFW/glfw3.h>
#include<glm/glm.hpp>
#include<glm/gtc/type_ptr.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<vector>
#include<iostream>
#include<fstream>
#include<sstream>
#include "model.h"

class castle
{
public:
	//dodac vector na modele
	std::vector<model> Models;
	float *tabVertex;
	float *tabNormals;
	float *tabTexCoords;
	long long int vertexCount;
	castle();
	~castle();
	//int LoadObj(const char * path);
	//void model_to_tabs(model);
	void read_vertexes(std::string);
	void read_normals(std::string);
	void read_texCoords(std::string);
	void read_all_models(std::string, int);
};

