#include "window.h"

glm::mat4 V = glm::mat4(1.0f);

window::window()
{
	aspectRatio = 1.0;
	glfwSetErrorCallback(this->error_callback);//Zarejestruj procedur� obs�ugi b��d�w

	

	if (!glfwInit()) { //Zainicjuj bibliotek� GLFW
		fprintf(stderr, "Nie mo�na zainicjowa� GLFW.\n");
		exit(EXIT_FAILURE);
	}


	//tworzenie okna
	//glfwWindowHint(GLFW_DECORATED, GL_FALSE);
	window_windu = glfwCreateWindow(1080, 720, "Castle", NULL, NULL);
	

	if (!window_windu) //Je�eli okna nie uda�o si� utworzy�, to zamknij program
	{
		fprintf(stderr, "Nie mo�na utworzy� okna.\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}


	glfwMakeContextCurrent(window_windu); //Od tego momentu kontekst okna staje si� aktywny i polecenia OpenGL b�d� dotyczy� w�a�nie jego.

	glfwSwapInterval(1); //Czekaj na 1 powr�t plamki przed pokazaniem ukrytego bufora

	if (glewInit() != GLEW_OK) { //Zainicjuj bibliotek� GLEW
		fprintf(stderr, "Nie mo�na zainicjowa� GLEW.\n");
		exit(EXIT_FAILURE);
	}

	// needed for glfwGetUserPointer to work
	glfwSetWindowUserPointer(window_windu, this);
	glfwSetWindowSizeCallback(window_windu, this->windowResizeCallback);
		
	glfwSetCursorPosCallback(window_windu, this->mouseCallback);

	//glfwSetKeyCallback(window_windu,this-> keyCallback);

	sp = new ShaderProgram("vertex.glsl", NULL, "fragment.glsl");
	
	glClearColor(0, 0.6, 1, 1);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);

	
	std::cout << "Castle initiated \n";


}

window::~window()
{
	//miejsce na czyszczenie pamieci
	delete this; 
	glfwDestroyWindow(window_windu); //Usu� kontekst OpenGL i okno
	glfwTerminate(); //Zwolnij zasoby zaj�te przez GLFW
	exit(EXIT_SUCCESS);
}

//Procedura obs�ugi b��d�w
void window::error_callback(int error, const char* description) {
	
	fputs(description, stderr);
	//Mo�na by da� catch
}



//Funkcja wczytuj�ca tekstur�
GLuint window::readTexture(const char* filename) {
	GLuint tex;
	glActiveTexture(GL_TEXTURE0);

	//Wczytanie do pami�ci komputera
	std::vector<unsigned char> image;   //Alokuj wektor do wczytania obrazka
	unsigned width, height;   //Zmienne do kt�rych wczytamy wymiary obrazka
	//Wczytaj obrazek
	unsigned error = lodepng::decode(image, width, height, filename);
	
	
	//Import do pami�ci karty graficznej
	glGenTextures(1, &tex); //Zainicjuj jeden uchwyt
	glBindTexture(GL_TEXTURE_2D, tex); //Uaktywnij uchwyt
	//Wczytaj obrazek do pami�ci KG skojarzonej z uchwytem
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, (unsigned char*)image.data());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	return tex;
}

void window::tex_for_cast() //xd nie dziala path
{

	tex0 = readTexture("bricks.png");
	tex1 = readTexture("sky.png");

	grass = readTexture("../Textures/grass2.png");
	walls = readTexture("../Textures/walls.png");
	doors = readTexture("../Textures/doors2.png");
	bulding = readTexture("../Textures/bulding2.png");

	fevers = readTexture("../Textures/texture000.png");
	fins = readTexture("../Textures/texture001.png");
	hat = readTexture("../Textures/texture003.png");
	head = readTexture("../Textures/texture004.png");
	face = readTexture("../Textures/texture005.png");
	coat = readTexture("../Textures/texture002.png");

	b_mask = readTexture("../Textures/batman/head.png");
	b_tors = readTexture("../Textures/batman/tors.png");
	b_cape = readTexture("../Textures/batman/cape.png");

	barell = readTexture("../Textures/barels/0.png");
	chest = readTexture("../Textures/barels/1.png");
	crossbow = readTexture("../Textures/barels/2.png");
	rack = readTexture("../Textures/barels/3.png");
	spear = readTexture("../Textures/barels/4.png");
	sword = readTexture("../Textures/barels/5.png");
}

void window::drawWhole(castle *stronghold, castle *donald, castle* batman, castle* weapons)
{
	
	glfwSetInputMode(window_windu, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	

	float currentFrame = glfwGetTime();
	timeBetweenFrames = currentFrame - lastFrame;
	lastFrame = currentFrame;
	keyboardReaction(window_windu);
	

	//Wylicz macierz widoku
	V = glm::lookAt(
		//(x,y,z)obort
		cameraPosition,
		//przesuniecie
		cameraPosition + cameraFront,
		//obr�t
		cameraUp);


	glm::mat4 P = glm::perspective(glm::radians(70.0f), aspectRatio, 1.0f, 5000.0f); //Wylicz macierz rzutowania

	sp->use();//Aktywacja programu cieniuj�cego

	//Przeslij parametry programu cieniuj�cego do karty graficznej P i V. M w ka�dym modelu osobno
	glUniformMatrix4fv(sp->u("P"), 1, false, glm::value_ptr(P));
	glUniformMatrix4fv(sp->u("V"), 1, false, glm::value_ptr(V));

	

	light_correction();
	
	this->draw_Stronghold(stronghold);
	if (darkness_mode)
	{
		this->draw_Batman(batman);
	}

	this->draw_Wepons(weapons);
	this->draw_Donald(donald);
	
	
	glfwSwapBuffers(window_windu);

}

void window::draw_Stronghold(castle *stronghold)
{

	glm::mat4 M = glm::mat4(1.0f);

	GLuint teksture;
	for (int i = 0; i < stronghold->Models.size(); i++)
	{
		
		float * verts = stronghold->Models[i].tabVertexes;
		float * normals = stronghold->Models[i].tabNormals;
		float * texCoords = stronghold->Models[i].tabTexCoords;
		long long int vertexCount = stronghold->Models[i].vertexCount;

		glUniformMatrix4fv(sp->u("M"), 1, false, glm::value_ptr(M));

		switch (i)
		{
		case 0: 
			teksture = walls;
			break;
		case 2:
			teksture = doors;
			break;
		case 1:
			teksture = grass;
			break;

		case 3:
			teksture = bulding;
			break;


		default:
			teksture = tex0;
			break;
		}
		
	
		glUniform1i(sp->u("textureMap0"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, teksture);

	

		glEnableVertexAttribArray(sp->a("vertex"));  //W��cz przesy�anie danych do atrybutu vertex
		glVertexAttribPointer(sp->a("vertex"), 4, GL_FLOAT, false, 0, verts); //Wska� tablic� z danymi dla atrybutu vertex

		glEnableVertexAttribArray(sp->a("normal"));  //W��cz przesy�anie danych do atrybutu normal
		glVertexAttribPointer(sp->a("normal"), 4, GL_FLOAT, false, 0, normals); //Wska� tablic� z danymi dla atrybutu normal

		glEnableVertexAttribArray(sp->a("texCoord0"));  //W��cz przesy�anie danych do atrybutu texCoord0
		glVertexAttribPointer(sp->a("texCoord0"), 2, GL_FLOAT, false, 0, texCoords); //Wska� tablic� z danymi dla atrybutu texCoord0d




		glDrawArrays(GL_TRIANGLES, 0, vertexCount); //Narysuj obiekt

		glDisableVertexAttribArray(sp->a("vertex"));  //Wy��cz przesy�anie danych do atrybutu vertex
		glDisableVertexAttribArray(sp->a("normal"));  //Wy��cz przesy�anie danych do atrybutu normal
		glDisableVertexAttribArray(sp->a("texCoord0"));  //Wy��cz przesy�anie danych do atrybutu texCoord0

	
	}

}

void window::draw_Donald(castle *donald)
{


	glm::mat4 M = glm::mat4(1.0f);
	M[3][0] = donald_position.x;
	M[3][1] = donald_position.y;
	M[3][2] = donald_position.z;
	M = glm::scale(M, glm::vec3(0.018f, 0.018f, 0.018f));
//	donald_Rotate_plz();
	M = glm::rotate(M, glm::radians(donald_angle), glm::vec3(0.0f, 1.0f, 0.0f));
	


	GLuint teksture;
	for (int i = 0; i < donald->Models.size(); i++)
	{

		float * verts = donald->Models[i].tabVertexes;
		float * normals = donald->Models[i].tabNormals;
		float * texCoords = donald->Models[i].tabTexCoords;
		long long int vertexCount = donald->Models[i].vertexCount;



//Przeslij parametry  M programu cieniuj�cego do karty graficznej
glUniformMatrix4fv(sp->u("M"), 1, false, glm::value_ptr(M));
glUniformMatrix4fv(sp->u("V"), 1, false, glm::value_ptr(V));


switch (i)
{
case 0:
	teksture = face;//ok
	break;
case 2:
	teksture = fins;  //ok
	break;
case 1:
	teksture = head; //ok
	break;
case 3:
	teksture = fevers; //ok
	break;
case 4:
	teksture = hat; //ok
	break;
case 5:
	teksture = coat;//ok
	break;
default:
	teksture = tex0;
	break;
}




glUniform1i(sp->u("textureMap0"), 0);
glActiveTexture(GL_TEXTURE0);
glBindTexture(GL_TEXTURE_2D, teksture);


glEnableVertexAttribArray(sp->a("vertex"));  //W��cz przesy�anie danych do atrybutu vertex
glVertexAttribPointer(sp->a("vertex"), 4, GL_FLOAT, false, 0, verts); //Wska� tablic� z danymi dla atrybutu vertex

glEnableVertexAttribArray(sp->a("normal"));  //W��cz przesy�anie danych do atrybutu normal
glVertexAttribPointer(sp->a("normal"), 4, GL_FLOAT, false, 0, normals); //Wska� tablic� z danymi dla atrybutu normal

glEnableVertexAttribArray(sp->a("texCoord0"));  //W��cz przesy�anie danych do atrybutu texCoord0
glVertexAttribPointer(sp->a("texCoord0"), 2, GL_FLOAT, false, 0, texCoords); //Wska� tablic� z danymi dla atrybutu texCoord0d




glDrawArrays(GL_TRIANGLES, 0, vertexCount); //Narysuj obiekt

glDisableVertexAttribArray(sp->a("vertex"));  //Wy��cz przesy�anie danych do atrybutu vertex
glDisableVertexAttribArray(sp->a("normal"));  //Wy��cz przesy�anie danych do atrybutu normal
glDisableVertexAttribArray(sp->a("texCoord0"));  //Wy��cz przesy�anie danych do atrybutu texCoord0
	}
}

void window::keyboardReaction(GLFWwindow * window) {
	float cameraSpeed = 10.0f * timeBetweenFrames;
	float sens = 1.0f;
	glm::vec3 front;
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
	}
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		cameraPosition += cameraSpeed * cameraFront;

	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		this->cameraPosition -= cameraSpeed * this->cameraFront;
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		this->cameraPosition -= cameraSpeed * glm::normalize(glm::cross(this->cameraFront, this->cameraUp));
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		this->cameraPosition += cameraSpeed * glm::normalize(glm::cross(this->cameraFront, this->cameraUp));
	}
	if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS) {
		std::cout << cameraPosition.x << " " << cameraPosition.y << " " << cameraPosition.z << std::endl;
		std::cout << cameraFront.x << " " << cameraFront.y << " " << cameraFront.z << std::endl;
	}
	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
		this->~window();
	}
	if (high) {
		if (cameraPosition.y < 2.0f) cameraPosition.y = 2.0f;
		if (cameraPosition.z > 16.8f) cameraPosition.z = 16.8f;
		if (cameraPosition.z < -7.2f) cameraPosition.z = -7.2f;
		if (cameraPosition.x > 17.6f) cameraPosition.x = 17.6f;
		if (cameraPosition.x < -19.16f) cameraPosition.x = -19.16f;
	}
	if ((cameraPosition.x > 17.5f || cameraPosition.x < -19.05f || cameraPosition.z < -7.1f || cameraPosition.z > 16.7f) && glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		if (dev_mode) {
			cameraPosition.y = 10.7;
			dev_mode = false;
		}
		high = false;
	}
	if (!((cameraPosition.x > 17.5f || cameraPosition.x < -19.05f || cameraPosition.z < -7.1f || cameraPosition.z > 16.7f) && glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)) {
		cameraPosition.y = 2.0f;
		dev_mode = true;
		high = true;
	}

	if (!darkness_mode) {
		if ((cameraPosition.x > 6.5f) && (cameraPosition.x < 10.0f) && (cameraPosition.z > 2.0f) && (cameraPosition.z < 5.18f) && glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
			darkness_mode = true;
			glClearColor(0.0f, 0.0f, 0.5f, 1.0f);//granat
			main_light_source = glm::vec4(-30.0f, 50.0f, 10.0f, 0.05f);
			secondary_light_source = glm::vec4(8.0f, 1.0f, 2.5f, 0.05f);
		}
	}
	else
	{
		if ((cameraPosition.x > 6.5f && cameraPosition.x < 10.0f && cameraPosition.z > 2.0f && cameraPosition.z < 5.18f) && glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
			darkness_mode = false;
			glClearColor(0.0f, 0.6f, 1.0f, 1.0f);
			main_light_source = default_main;
			secondary_light_source = default_secoundary_light;
		}
	}
}

void window::mCallback(GLFWwindow* window, double xpos, double ypos) {
	if (first_camera_move) {
		xCoor = xpos;
		yCoor = ypos;
		first_camera_move = false;
	}
	float xMove = xpos - xCoor;
	float yMove = yCoor - ypos;
	xCoor = xpos;
	yCoor = ypos;

	xMove *= sens;
	yMove *= sens;

	yaw += xMove;
	pitch += yMove;

	if (pitch > 89.0f) pitch = 89.0f;
	if (pitch < -89.0f) pitch = -89.0f;
	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	cameraFront = glm::normalize(front);
}

void window::draw_Batman(castle* batman)
{
	glm::mat4 M = glm::mat4(1.0f);
	GLuint teksture;
	for (int i = 0; i < batman->Models.size(); i++)
	{

		float * verts = batman->Models[i].tabVertexes;
		float * normals = batman->Models[i].tabNormals;
		float * texCoords = batman->Models[i].tabTexCoords;
		long long int vertexCount = batman->Models[i].vertexCount;

		//Przeslij parametry  M programu cieniuj�cego do karty graficznej
		glUniformMatrix4fv(sp->u("M"), 1, false, glm::value_ptr(M));


		switch (i)
		{
		case 2:
			teksture = b_mask;//ok
			break;
		case 0:
			teksture = b_tors;  //ok
			break;
		case 1:
			teksture = b_cape; //ok
			break;
		default:
			teksture = tex0;
			break;
		}


		glUniform1i(sp->u("textureMap0"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, teksture);

		//dorobi� wet shader dodatkowy shader


		glEnableVertexAttribArray(sp->a("vertex"));  //W��cz przesy�anie danych do atrybutu vertex
		glVertexAttribPointer(sp->a("vertex"), 4, GL_FLOAT, false, 0, verts); //Wska� tablic� z danymi dla atrybutu vertex

		glEnableVertexAttribArray(sp->a("normal"));  //W��cz przesy�anie danych do atrybutu normal
		glVertexAttribPointer(sp->a("normal"), 4, GL_FLOAT, false, 0, normals); //Wska� tablic� z danymi dla atrybutu normal

		glEnableVertexAttribArray(sp->a("texCoord0"));  //W��cz przesy�anie danych do atrybutu texCoord0
		glVertexAttribPointer(sp->a("texCoord0"), 2, GL_FLOAT, false, 0, texCoords); //Wska� tablic� z danymi dla atrybutu texCoord0d


		glDrawArrays(GL_TRIANGLES, 0, vertexCount); //Narysuj obiekt

		glDisableVertexAttribArray(sp->a("vertex"));  //Wy��cz przesy�anie danych do atrybutu vertex
		glDisableVertexAttribArray(sp->a("normal"));  //Wy��cz przesy�anie danych do atrybutu normal
		glDisableVertexAttribArray(sp->a("texCoord0"));  //Wy��cz przesy�anie danych do atrybutu texCoord0
		
		
	}

}

void window::draw_Wepons(castle *wepons)
{

	glm::mat4 M = glm::mat4(1.0f);
	M = glm::translate(M, glm::vec3(-10.0f,0.05f, 12.0f));
	
	GLuint teksture;
	for (int i = 0; i < wepons->Models.size(); i++)
	{

		float * verts = wepons->Models[i].tabVertexes;
		float * normals = wepons->Models[i].tabNormals;
		float * texCoords = wepons->Models[i].tabTexCoords;
		long long int vertexCount = wepons->Models[i].vertexCount;



		//Przeslij parametry  M programu cieniuj�cego do karty graficznej
		glUniformMatrix4fv(sp->u("M"), 1, false, glm::value_ptr(M));


		switch (i)
		{
		case 2://skrzynia
		case 10: //skrzynia
			teksture = chest;
			break;
		case 5:
			teksture = rack;  
			break;
		case 4://cr
			teksture = crossbow; //ok
			break;
		case 0: //sword on rack
		case 6: //miecz
		case 11://miecz
		case 9: //sword on rack
			teksture = sword; 
			break;
		case 7: //spear
		case 1: //spear
			teksture = spear; 
			break;
		case 8: //barel
		case 3: //barel
			teksture = barell;
			break;
		default:
			teksture = tex1;
			break;
		}




		glUniform1i(sp->u("textureMap0"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, teksture);


		glEnableVertexAttribArray(sp->a("vertex"));  //W��cz przesy�anie danych do atrybutu vertex
		glVertexAttribPointer(sp->a("vertex"), 4, GL_FLOAT, false, 0, verts); //Wska� tablic� z danymi dla atrybutu vertex

		glEnableVertexAttribArray(sp->a("normal"));  //W��cz przesy�anie danych do atrybutu normal
		glVertexAttribPointer(sp->a("normal"), 4, GL_FLOAT, false, 0, normals); //Wska� tablic� z danymi dla atrybutu normal

		glEnableVertexAttribArray(sp->a("texCoord0"));  //W��cz przesy�anie danych do atrybutu texCoord0
		glVertexAttribPointer(sp->a("texCoord0"), 2, GL_FLOAT, false, 0, texCoords); //Wska� tablic� z danymi dla atrybutu texCoord0d




		glDrawArrays(GL_TRIANGLES, 0, vertexCount); //Narysuj obiekt

		glDisableVertexAttribArray(sp->a("vertex"));  //Wy��cz przesy�anie danych do atrybutu vertex
		glDisableVertexAttribArray(sp->a("normal"));  //Wy��cz przesy�anie danych do atrybutu normal
		glDisableVertexAttribArray(sp->a("texCoord0"));  //Wy��cz przesy�anie danych do atrybutu texCoord0
	}
}

void window::light_correction()
{
	glUniform1f(sp->u("lpower"), main_light_source.w);
	//glUniform4f(sp->u("light_dir1"),  main_light_source.x, main_light_source.y, main_light_source.z, 0.0f); 


	glUniform1f(sp->u("l2power"), secondary_light_source.w); 
	//glUniform4f(sp->u("light_dir2"), -0.85f-secondary_light_source.x,-2.0f- secondary_light_source.y , -7.2f-secondary_light_source.z, 0.0f); 

	if (darkness_mode) {
		glUniform4f(sp->u("C_direction"), 0.0f, -0.01f, -1.1f, 0.0f);
		glUniform1f(sp->u("radius"), glm::cos(glm::radians(8.0f)));
		glUniform4f(sp->u("lp2"), window_light_source.x, window_light_source.y, window_light_source.z, window_light_source.w); //Wsp�rz�dne �r�d�a �wiat�a
	}
	else
	{
		glUniform1f(sp->u("radius"), glm::cos(glm::radians(0.0f)));
	}

}

void window:: donald_Rotate_plz()
{

	//TODO
	float length = sqrt(pow((cameraPosition.x - donald_position.x),2)  + pow((cameraPosition.z - donald_position.z) ,2));
	float z_distance = cameraPosition.z - donald_position.z;
	donald_angle = asin(z_distance / length);
	

}