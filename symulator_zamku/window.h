#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "lodepng.h"


#include "shaderprogram.h"
#include "castle.h"



class window
{
public:

	ShaderProgram *sp;
	float aspectRatio;
	GLFWwindow * window_windu;

	//batman activate
	bool darkness_mode = false;
	bool high = true;
	bool dev_mode = true;
	//KAMERA
	float timeBetweenFrames = 0.1f;
	float lastFrame = 0.0f;


	//wsporzedne kamery
	glm::vec3 cameraPosition = glm::vec3(-0.33f, 2.9f, -5.5f);
	glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, 10.0f);
	glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);

	float xCoor = 1080.0f / 2.0;
	float yCoor = 720.0 / 2.0;
	float pitch = 0.0f; //pitch
	float yaw = 90.0f;	//yaw
	float sens = 0.1f;
	bool first_camera_move = true;


	//�wiat�o g��wne
	glm::vec4 default_main = glm::vec4(-20.0f, 50.0f, 20.0f, 1.3f);
	glm::vec4 main_light_source = default_main;
	
	
	//�wiat�o pomocnicze
	glm::vec4 default_secoundary_light = glm::vec4(13.0f, 5.0f, 5.0f, 1.1f);
	glm::vec4 secondary_light_source = default_secoundary_light;

	
	//�wiat�o na batmana
	glm::vec4 window_light_source = glm::vec4(0.0f, 10.0f, 16.0f, -1.0f);

	glm::vec3 donald_position = glm::vec3(9.0f, 0.38f, 4.5f);
	float donald_angle = -90.0f;
	

	
	//Uchwyty na tekstury
	GLuint tex0;
	GLuint tex1;

	GLuint grass;
	GLuint walls;
	GLuint doors;
	GLuint bulding;

	GLuint car;

	GLuint fevers;
	GLuint fins;
	GLuint coat;
	GLuint hat;
	GLuint head;
	GLuint face;

	GLuint b_mask;
	GLuint b_tors;
	GLuint b_cape;

	GLuint barell;
	GLuint chest;
	GLuint crossbow;
	GLuint rack;
	GLuint spear;
	GLuint sword;



	window();
	~window();

	inline static void windowResizeCallback(GLFWwindow *_window, int width, int height)
	{
		if (height == 0) return;
		window *Win = static_cast<window*>(glfwGetWindowUserPointer(_window));
		glViewport(0, 0, width, height);
		Win->changeA((float)width / (float)height);
	}

	void changeA(float a) {
		aspectRatio = a;
	};

	static void error_callback(int error, const char * description);


	//do usuniecia
	inline static void keyCallback(GLFWwindow * _window, int key, int scancode, int action, int mods) {
		window *Win = static_cast<window*>(glfwGetWindowUserPointer(_window));
		//ACTION

	};




	void drawWhole(castle *, castle *, castle *, castle *);

	void light_correction();

	void donald_Rotate_plz();

	void draw_Stronghold(castle * stronghold);



	void tex_for_cast();

	GLuint readTexture(const char * filename);

	void keyboardReaction(GLFWwindow*);

	
	void draw_Donald(castle * donald);

	void draw_Batman(castle * batman);

	void draw_Wepons(castle * wepons);

	inline static void mouseCallback(GLFWwindow * _window, double xpos, double ypos) {
		window *Win = static_cast<window*>(glfwGetWindowUserPointer(_window));
		Win->mCallback(_window, xpos, ypos);
	};

	void mCallback(GLFWwindow*, double, double);


};

