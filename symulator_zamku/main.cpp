#pragma warning(disable : 4996)
#include<fstream>
#include<string>
#include<GL/glew.h>
#include<GLFW/glfw3.h>
#include<glm/glm.hpp>
#include<glm/gtc/type_ptr.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<vector>
#include<iostream>
#include "window.h"




int main()
{

	window game;


	castle *stronghold = new castle();
	castle *donald = new castle();
	castle *batman = new castle();
	castle *weapons = new castle();


	//stronghold->read_normals("../modele_zamku/caer/normals0.txt");
	//stronghold->read_texCoords("../modele_zamku/caer/texCoords0.txt");
	//stronghold->read_vertexes("../modele_zamku/caer/vertex0.txt");




	//glfwSetTime(0); //Zeruj timer

	//odczyt zamku

	game.tex_for_cast();
	stronghold->read_all_models("../modele_zamku/zamke_projektowy/", 3);
	donald->read_all_models("../modele_zamku/donald/s/", 5);
	batman->read_all_models("../modele_zamku/dark/s/", 2);
	weapons->read_all_models("../modele_zamku/barrels/s/", 11);
	
	glfwSetTime(0);
	while (!glfwWindowShouldClose(game.window_windu))
	{
		
		game.drawWhole(stronghold, donald, batman, weapons);

		glfwPollEvents();

	}


	system("pause");




}