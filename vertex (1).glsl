#version 330

//Zmienne jednorodne
uniform mat4 P;
uniform mat4 V;
uniform mat4 M;
uniform vec4 swiatlo;
//Atrybuty
in vec4 vertex; //wspolrzedne wierzcholka w przestrzeni modelu
in vec4 normal; //wspolrzedne wektora normalnego w przestrzeni modelu
in vec4 color; //kolor wierzchołka

//Zmienne interpolowane
out vec4 ic;
out vec4 l;
out vec4 n;
out vec4 v;

void main(void) {

    /*vec4 kd=color; //Kolor obiektu
    vec4 ld=vec4(1,1,1,1); //Kolor swiatla
    vec4 ks = vec4(1,1,1,1);//kolor materialu swiattla odbitego
    vec4 ls = vec4(1,1,1,1);//kolor światła odbitego*/

    vec4 lp= swiatlo + vec4(0,0,-1,0);//vec4(0,0,-6,1);

    l=normalize(V*(lp-M*vertex)); //Wektor "do swiatla" w przestrzeni oka
    n=normalize(V*M*normal); //Wektor normalny w przestrzeni oka
    v = normalize(vec4(0,0,0,1) - V*M*vertex);// 0,0,0,1 to przestrzen



    //vec4 r = reflect(-l,n);//wzor z modelu Phonga wektor jest w przestrzeni oka



   // vec4 lp=vec4(0,0,-6,1);//Wspó³rzêdne Ÿród³a swiatla w przestrzeni œwiata



    //float nl=clamp(dot(n,l),0,1);//dot zwraca iloczyn skalarny
    //float rv=pow(clamp(dot(r,v),0,1), 35);

    ic=color;
    //ic=vec4(kd.rgb*ld.rgb*nl + ks.rgb*ls.rgb*rv,kd.a);
    gl_Position=P*V*M*vertex;
}
