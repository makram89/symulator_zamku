#version 330


//Zmienne interpolowane
in vec4 ic;
in vec4 l;
in vec4 n;
in vec4 v;

out vec4 pixelColor; //Zmienna wyjsciowa fragment shadera. Zapisuje sie do niej ostateczny (prawie) kolor piksela

void main(void) {
    vec4 kd=ic; //Kolor obiektu
    vec4 ld=vec4(1,1,1,1); //Kolor swiatla
    vec4 ks = vec4(1,1,1,1);//kolor materialu swiattla odbitego
    vec4 ls = vec4(1,1,1,1);//kolor �wiat�a odbitego

    vec4 ml = normalize(l);
    vec4 mn = normalize(n);
    vec4 mv = normalize(v);

    vec4 r = reflect(-ml,mn);//wzor z modelu Phonga wektor jest w przestrzeni oka

    float nl=round(clamp(dot(ml,ml),0,1)*4)/4;//dot zwraca iloczyn skalarny
    float rv=pow(clamp(dot(r,mv),0,1), 35);

	pixelColor=vec4(kd.rgb*ld.rgb*nl + ks.rgb*ls.rgb*rv,kd.a);
	//pixelColor=ic;
}
